from flask import Flask
from flask import request
application = Flask(__name__)
import os
import json as js
import app.db_helpers as db
import globals as g
from app.preprocessing import preprocessing_main as prep
from app.preprocessing import strict_preprocessing as strict_prep
from app.preprocessing import helpers as help
from app.extractor import extractor_main as extractor
from app.postprocessing import postprocessing_main as post
import app.network_trainer as save_network

@application.route('/extract', methods=['GET', 'POST'])
def extract():
    # data = get_test_single()
    data = js.loads(request.get_data().decode('utf-8'))
    results = []
    for doc in data:
        agency = doc['agency']
        event_id = doc['event_id']
        document = doc['document']
        reimbursement_disease_condition_guesses = extractor.extract(agency, "diseases", document)
        drugs_guesses = extractor.extract(agency, "drugs", document)
        guess = {
            "event_id" : event_id,
            "disease_conditions" : reimbursement_disease_condition_guesses,
            "drugs" : drugs_guesses,
        }
        results.append(guess)
    return js.dumps(results)

def get_test_single():
    id = 1349
    agency = 5
    all = db.get_and_save_or_open(str(agency), 'training_data.npy', 'training_data/' + str(agency))
    for x in all:
        if x['event_id'] == id:
            data = x
    data['agency'] = agency
    data = [ data ]
    return data

@application.route('/test/<int:agency>/<string:type>/<string:model>', methods=['GET'])
def test(agency, type, model):
    results = extractor.test_extract(agency, type, model)
    results = post.add_readable_stats(results, type)
    tallies = post.tally_guesses(results)
    return js.dumps([tallies] + results)

@application.route('/refresh/<int:agency>/<string:type>/<string:model>', methods=['GET'])
def refresh(agency, type, model):
    if model == 'strict':
        strict = True
    else:
        strict = False
    accuracy = save_network.run_it(agency, type, strict)
    return 'accuracy: ' + str(accuracy)

@application.route('/supported_agencies', methods=['GET'])
def supported_agencies():
    exclusions = ['.DS_Store', '__pycache__', 'helpers.py']
    agencies = [ agency for agency in os.listdir(g.G.DB_PATH) if agency not in exclusions]
    return js.dumps(agencies)

@application.route('/load/<int:agency>/<string:type>/<string:model>', methods=['GET'])
def load(agency, type, model):
    agency = str(agency)
    data = db.get_and_save_or_open(agency, 'training_data.npy', 'training_data/' + agency)
    if len(data) < 50:
        return 'Insufficient data to build model; loaded, but will only return frequency results'

    training_data, test_data = help.split_it(data)

    if model == 'strict':
        training_filename = 'formatted_strict_training.npy'
        testing_filename = 'formatted_strict_testing.npy'
        strict = True
        prepper = 'strict_prep'
    else:
        training_filename = 'formatted_training.npy'
        testing_filename = 'formatted_testing.npy'
        strict = False
        prepper = 'prep'

    pick_prep = {'strict_prep': strict_prep, 'prep': prep}
    prepper = pick_prep[prepper]

    formatted_training = prepper.format_data(training_data, type, training=True)
    formatted_testing = prepper.format_data(test_data, type)

    db.save_it(agency + '/' + type, training_filename, formatted_training)
    db.save_it(agency + '/' + type, testing_filename, formatted_testing)

    model_accuracy = save_network.run_it(agency, type, strict)

    return js.dumps('model_accuracy: ' + str(model_accuracy))

application.run(debug=True,host='0.0.0.0', port=80)
