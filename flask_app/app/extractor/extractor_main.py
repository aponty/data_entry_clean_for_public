from app.extractor import network_extractor
from app.extractor import frequency_extractor
import app.db_helpers as db
from app.preprocessing import preprocessing_main as prep
from app.preprocessing import strict_preprocessing as strict_prep
import globals as g
MINIMUM_DOCS_FOR_MODELBUIDING = 50

def test_extract(agency, type, model):
    if model == 'strict':
        formatted_all = db.open_it(agency, type, '/formatted_strict_training.npy')
        strict = True
        extractor = network_extractor
    elif model == 'non':
        formatted_all = db.open_it(agency, type, '/formatted_training.npy')
        strict = False
        extractor = network_extractor
    elif model == 'frequency':
        formatted_all = db.get_and_save_or_open(agency, 'training_data.npy', 'training_data/' + str(agency))
        extractor = frequency_extractor
        strict = None

    results = []
    for i, formatted_single in enumerate(formatted_all):
        print('on: ' + str(i))
        results.append(extractor.extract(agency, type, formatted_single, strict))
    return results

def extract(agency, type, document):
    if has_training_data( agency):
        strict_formatted = strict_prep.format_datum(document, type)
        strict_guesses = [ guess['guess_id'] for guess in network_extractor.extract(agency, type, strict_formatted, True)['guesses'] if guess['prediction']]

        formatted = prep.format_datum(document, type)
        non_strict_guesses = [ guess['guess_id'] for guess in network_extractor.extract(agency, type, formatted, False)['guesses'] if guess['prediction']]
    else:
        strict_guesses = []
        non_strict_guesses = []
    frequency_guesses = frequency_extractor.get_guess(document, type)
    guesses = {
        'non': non_strict_guesses,
        'strict': strict_guesses,
        'frequency': frequency_guesses
    }
    return guesses

def has_training_data(agency):
    return len(db.get_and_save_or_open(agency, 'training_data.npy', 'training_data/' + str(agency))) > MINIMUM_DOCS_FOR_MODELBUIDING
