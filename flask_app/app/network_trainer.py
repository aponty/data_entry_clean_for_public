import tensorflow as tf
import app.db_helpers as db
import numpy as np
import globals as g

def get_batch(arr, step, batch_size):
    start_index = batch_size * (step - 1)
    end_index = batch_size * step
    return arr[start_index:end_index]

def make_csv(data, agency, type, name):
    positions = [x['document'] for x in data]
    positions = [x for y in positions for x in y]
    positions = [[x['correct']] + x['positions'].tolist() for x in positions]
    import csv
    with open(agency+type+name, 'w') as myfile:
        wr = csv.writer(myfile, quoting=csv.QUOTE_ALL)
        wr.writerows(positions)

def make_batchable(data):
    # this is obviously not the most efficient way to get these numbers, fix when headache is gone
    abs_count = [[ doc['positions'] for i, doc in enumerate(event['document'])] for event in data ]
    abs_count = [x for y in abs_count for x in y ]
    abs_count = len(abs_count)

    correct_count = [[ doc['positions'] for i, doc in enumerate(event['document']) if doc['correct']] for event in data ]
    correct_count = [x for y in correct_count for x in y ]
    correct_count = len(correct_count)

    correct_ratio = abs_count/correct_count
    print('ratio ' + str(abs_count/correct_count))

    xs = [[ doc['positions'] for doc in event['document']] for event in data ]
    ys = [[ doc['correct'] for doc in event['document'] ] for event in data ]
    xs = [x for y in xs for x in y ]
    ys = [x for y in ys for x in y ]
    ys = [ [0.,1.] if y else [1., 0.] for y in ys ]
    xs = [ np.array(x) for x in xs ]
    ys = [ np.array(y) for y in ys ]
    return xs, ys, correct_ratio

def hyperparameters(event_count):
    num_steps       = 500
    batch_size      = int(event_count/num_steps)
    display_step    = int(num_steps/5)

    if batch_size == 0:
        batch_size      = 2
        num_steps       = int(event_count/batch_size)
        display_step    = int(num_steps/5)
    print('number of events is: ' + str(event_count) + '\nnumbers of steps is: ' + str(num_steps) + '\nfor a batch size of: ' + str(batch_size))
    return num_steps, batch_size, display_step

def run_it(agency, type, strict=False, attempts=0):

    agency = str(agency)

    if strict:
        training_data = db.open_it(agency, type, 'formatted_strict_training.npy')
        testing_data = db.open_it(agency, type, 'formatted_strict_testing.npy')
    else:
        training_data = db.open_it(agency, type, 'formatted_training.npy')
        testing_data = db.open_it(agency, type, 'formatted_testing.npy')

    # make_csv(training_data, agency, type, 'training.csv')
    # make_csv(testing_data, agency, type, 'testing.csv')

    xs, ys, correct_ratio =  make_batchable(training_data)
    test_xs, test_ys, test_ratio =  make_batchable(testing_data)

    towards_false_postivies_skew = 1
    correct_ratio = correct_ratio * towards_false_postivies_skew

    # Network Parameters
    num_input   = len(xs[0]) # 100
    n_hidden_1  = num_input # 1st layer number of neurons
    n_hidden_2  = num_input # 2nd layer number of neurons
    num_classes = 2 # result classes; # [0,1] == true and  [1, 0] == false. Couldn't get one t/f output neuron to work

    # Hyperparameters
    learning_rate   = 0.1
    event_count     = len(xs)
    num_steps, batch_size, display_step = hyperparameters(event_count)

    # tf Graph input; token locations
    X = tf.placeholder(dtype=tf.float32, shape=[None, num_input], name='x')
    # tf Graph output; predictions
    Y = tf.placeholder(dtype=tf.float32, shape=[None, num_classes], name='y')

    weights = {
        'h1': tf.Variable(tf.random_normal([num_input, n_hidden_1]), name='h1'),
        'h2': tf.Variable(tf.random_normal([n_hidden_1, n_hidden_2]), name='h2'),
        'out': tf.Variable(tf.random_normal([n_hidden_2, num_classes]), name='weight_out')
    }
    biases = {
        'b1': tf.Variable(tf.random_normal([n_hidden_1]), name='b1'),
        'b2': tf.Variable(tf.random_normal([n_hidden_2]), name='b2'),
        'out': tf.Variable(tf.random_normal([num_classes]),name='bias_out')
    }

    def neural_net(x):
        layer_1 = tf.add(tf.matmul(x, weights['h1']), biases['b1'])
        layer_2 = tf.add(tf.matmul(layer_1, weights['h2']), biases['b2'])
        out_layer = tf.add(tf.matmul(layer_2, weights['out']), biases['out'])
        return out_layer

    logits = neural_net(X)
    prediction = tf.nn.softmax(logits)

    loss_op = tf.reduce_mean(
        tf.nn.weighted_cross_entropy_with_logits( # because uneven data set. Otherwise just trains to say 'false' for everything, as that gives 90+ accuracy
            targets=Y,
            logits=logits,
            pos_weight=correct_ratio
        )
    )
    optimizer = tf.train.AdamOptimizer(learning_rate=learning_rate)

    train_op = optimizer.minimize(loss_op)

    correct_pred = tf.equal(tf.argmax(prediction, 1), tf.argmax(Y, 1))
    accuracy = tf.reduce_mean(tf.cast(correct_pred, tf.float32))

    saver = tf.train.Saver()

    with tf.Session() as sess:
        sess.run(tf.global_variables_initializer())
        for step in range(1, num_steps+1):
            batch_x, batch_y = get_batch(xs, step, batch_size), get_batch(ys, step, batch_size)
            # Run optimization op (backprop)
            sess.run(train_op, feed_dict={X: batch_x, Y: batch_y})
            if step % display_step == 0 or step == 1:
                # Calculate batch loss and accuracy
                loss, acc = sess.run([loss_op, accuracy], feed_dict={X: batch_x, Y: batch_y})
                print("Step " + str(step) + ", Minibatch Loss= " + \
                      "{:.4f}".format(loss) + ", Training Accuracy= " + \
                      "{:.3f}".format(acc))

        testing_accuracy = sess.run(accuracy, feed_dict={X: test_xs, Y: test_ys})
        print('overall accuracy: ' + str(testing_accuracy))

        if strict:
            export_path = g.G.DB_PATH + agency + '/' + type + '/strict_model/model'
        else:
            export_path = g.G.DB_PATH + agency + '/' + type + '/model/model'
        saver.save(sess, export_path)
    tf.reset_default_graph()
    return testing_accuracy
