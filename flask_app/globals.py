class G: #gotta wrap them in a class because circular imports. Otherwise a module would be fine.
    from joblib import Memory
    import os
    mem = Memory(cachedir='/tmp/joblib')

    DB_PATH = os.path.dirname(os.path.abspath(__file__)) + '/db/'
    # DB_PATH = '/Users/apontynen/code/work/data_entry_model/flask_app/db'

    @mem.cache
    def drugs():
        import app.db_helpers as db
        from app.preprocessing import helpers as help
        exclusions = ['placebo', 'tace', 'til', 'surgery', 'bsc', 'soc', "extended",
        "unspecified", "sodium", "alcohol", "chloride", "sham", "Based on sensitivity analysis",
        "optimal symptomatic treatment ", "chemotherapy", "therapeutic autologous dendritic cells",
        "mumps virus strain b level jeryl lynn live antigen", "house dust mite allergen immunotherapy",
        "Low dose doxorubicin", "acetic", "GSK3359609 IV infusion", "TTI-622 Monotherapy",
        "IRX-2 Regimen", "Oral CC - 486", "Saline solution", "vehicle control", "pooled human plasma",
        "varicella zoster virus (live)", "modified vaccinia virus ankara vaccine expressing p53", "bcg bladder cancer vaccine",
        "release dipyridamole", "PART 1: PT2385 Tablets", "eye wash", "sodium , dibasic", "factor xiii concentrate (human)",
        "Normal saline", "glucose indicator blood", "IRX-2 Regimen", "tar blend shampoo", "t cells",
        "IRX-2 Regimen", "antihemophilic factor, human recombinant residues 743-1636 deleted", "nant ovarian cancer vaccine",
        "metformin ,", "Part 1: BO-112", "DSP-7888 Dosing Emulsion", "best supportive care", "pilimumab", 'aspirin', "platinum",
        "acid"
        ]
        drugs = [drug for drug in db.get('drugs') if drug['name'] not in exclusions and len(drug['name']) < 50 ]
        drug_list = [ help.tokenize_sentence(drug['name']) for drug in drugs ]
        return drug_list, drugs

    @mem.cache
    def diseases():
        import app.db_helpers as db
        from app.preprocessing import helpers as help
        diseases = db.get('disease_conditions_with_skus')
        disease_acronyms = { # spaces keep acronyms from matching vs parts of words; ' UC ' is not in 'fadfsdluc'
            " HIV ": 11,
            " CF ": 23,
            " PAH ": 157,
            " RA ": 35,
            " HCC ": 57,
            "Renal Cancer": 57,
            "Lennox-Gastaut": 30,
            " AML ": 62,
            " CLL ": 64,
            " MDS ": 74,
            " GIST ": 59,
            " NHL ": 66,
            " COPD ": 33,
            " NSCLC ": 43,
            " MTC ": 92,
            "Head and Neck": 133,
            'Chronic Myelogenous Leukemia': 7,
            'Chronic Myeloid Leukemia': 7,
            ' CML ': 7,
            " MS ": 21,
            "Obesity": 188,
            " UC ": 25,
            " CINV ": 274,
            " PAD ": 51,
            " ACS ": 48,
            " IPF ": 89,
            " HCV ": 5,
            "Hep C": 5
        }
        res = []
        for disease in diseases:
            if '- Pediatric' not in disease['name']: # Model can't differentiate between peds and adults. Just remove them all
                if 'Liver' not in disease['name'] and "Kidney" not in disease['name']: # too common of terms, gotta leave them as 'Liver Cancer' etc
                    disease['name'] = disease['name'].replace(' - Adults', '').replace(' - Adult Type 2 - Injectable', '').replace(" - Adult Type 2 - Oral Medications", '').replace(' Cancer', '')
                    res.append(disease)
        for alt in disease_acronyms:
            res.append({
                'id' : disease_acronyms[alt],
                'name' : alt,
                'alias' : alt,
                'type': "DiseaseCondition"
            })
        diseases = res
        disease_condition_list = [ help.tokenize_sentence(disease['name']) for disease in diseases ]
        return disease_condition_list, diseases
