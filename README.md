# data_entry_clean_for_public

A copy of a repo, with private info redacted. Models are not included as they are proprietary (work creation). So, not runnable, but evidence of work.

This started off a project to learn Python, and I decided to try and go with a more functional style instead of object-oriented. Because Reasons™️. Not necessarily good ones, but it seemed interesting at the time. Famous last words, right?

Anyways, it got great results, but there is unfortunately some spagetti mixed in there. Live and learn! 🤷 


# data_entry_model

## flask_app

A dockerized flask app running here- [http://dataentry-env.us-east-1.elasticbeanstalk.com/](http://dataentry-env.us-east-1.elasticbeanstalk.com/)

### Deployment:
Deployment is based on uploading the dockerfile to ElasticBeanstalk. The code gets into the container via a git clone command that pulls the latest version of the master branch of the repo. So, just push/merge changes to github and then [redeploy the dockerfile here](https://console.aws.amazon.com/elasticbeanstalk/home?region=us-east-1#/environment/dashboard?applicationName=data_entry&environmentId=e-nittmv28vx)
(command line instructions tbd)

### For local development:

Comment out git call in Dockerfile and uncomment the local copy command in order to point the code in the container to local directories instead of github

`docker build -t flask_app .` in app root to build image    
`docker run -it --entrypoint "/bin/sh" flask_app`   to overide python entrypoint and get into a shell    
`docker run -p 80:80 flask_app`                 to run locally    

Update paths in scripts in `test` directory to whatever they are on your machine, then paste the get_data.rb file into console to load files to test off of

`ruby run.rb smc extract` or `ruby run.rb smc load_agency` to test

look at response.txt in test folder to see response

### App API:

App has two routes:

#### load_agency:  
##### POST request:
```javascript
{ "agency": "agency_name",
"data" : [
    "indication sentence",
    "indication sentence"
    ]
}
```

#### extract:
##### POST Request:
```javascript
{ "agency": "agency_name",
"data" : {
    "event_id" : "id",
    "doc_body" : "full text of document as a string"
}
}
```

##### Response:
```javascript
{ "event_id": "event_id",
"indication_guesses" : [
    "guess_1",
    "guess_2",
    "guess_3"
]
}
```

## lis_model directory

Data extractor in script form. Less overhead to worry about, easier for model development. This directory (and the test directories) are deleted from the production app.    

Edit path in `get_data.rb` to wherever you cloned this repo.

Edit AGENCY and AGENCY_ID in `get_data.rb`

copy that code and run it in rails console

Install python3 and necessary imports (just genism and textblob, I think). Use pip3 and python3 commands to avoid version issues

run find_indication.py passing in agency name as arg; `python3 find_indication.py nice`


### ISSUES:
(non-prioritized list)    
1) running flask development server, should upgrade to gunicorn    
2) python 3.5 instead of 3.6    
3) app structure is not finalized/should be reworked for expandability
      3.5) agency options should be double checked as optimal
4) routes don't match system names    
5) ADD api call in dockerfile to invalidate cache is a hack    
6) should add batch route to speed things up
