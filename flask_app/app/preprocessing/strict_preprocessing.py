from app.preprocessing import helpers as help
import globals as g

def format_data(data, type, training=False):
    data = [{
                'event_id': event['event_id'],
                'drugs': event['drugs'],
                'diseases': event['diseases'],
                'document': document_formatter(event['document'], type, event[type])
            } for event in data ]
    return data

def format_datum(document, type):
    data = {
                'event_id': [],
                'drugs': [],
                'diseases': [],
                'document': document_formatter(document, type)
            }
    return data

def document_formatter(document, type, correct_ids=[]):
    pick_type = {'diseases': g.G.diseases, 'drugs': g.G.drugs}
    options = pick_type[type]()[1]

    document = [ insert_id(sentence, options) for sentence in help.split_to_sentences(document)]
    document = help.split_by_drug(document)
    document = [ {
        'guess_id': doc['guess_id'],
        'positions': help.resample(doc['positions']).tolist(),
        'correct': doc['guess_id'] in correct_ids # always false for single extractions as we don't already know the correct answer for those
        } for doc in document ]
    return document

def insert_id(sentence, options):
    ids = []
    for option in options:
        if option['name'].lower() in sentence:
            ids.append(option['id'])
    return ids
