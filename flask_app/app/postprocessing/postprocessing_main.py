import globals as g

def add_readable_stats(results, type):
    pick_type = {'diseases': g.G.diseases, 'drugs': g.G.drugs}
    options = pick_type[type]()[1]
    detailed_results = []
    for res in results:
        guesses = [
                {
                    'guess': [ disease['name'] for disease in options if disease['id'] == guess['guess_id']],
                    "prediction": guess['prediction'],
                    'correct': guess['correct']
                } for guess in res['guesses'] if guess['prediction']]
        detailed_results.append({
            'event_id': res['event_id'],
            type: [ x['name'] for x in options if x['id'] in res[type]],
            'guesses': guesses
        })
    return detailed_results


def tally_guesses(results):
    tallies = {
        'correct positives': 0,
        'correct negatives': 0,
        'false positives': 0,
        'false negatives': 0,
        'no guesses': 0
    }

    for event in results:
        if len(event['guesses']) == 0:
            tallies['no guesses'] += 1
        for guess in event['guesses']:
            if guess['prediction'] == False:
                if guess['correct'] == False:
                    tallies['correct negatives'] += 1
                else:
                    tallies['false negatives'] += 1
            else:
                if guess['correct'] == True:
                    tallies['correct positives'] += 1
                else:
                    tallies['false positives'] += 1
    tallies['total events'] = len(results)
    tallies['effective accuracy'] = str(round((tallies['correct positives'] / (tallies['correct positives'] + tallies['false positives'] + tallies['false negatives']))*100)) + "%"
    tallies['appx global accuracy'] = str(round(tallies['correct positives'] / (tallies['total events'] )*100)) + "%"
    return tallies
