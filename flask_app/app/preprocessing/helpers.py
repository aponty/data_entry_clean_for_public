import re
from textblob import TextBlob

def tokenize_sentence(string):
    string = re.sub("[^a-zA-Z\s]", "", string).strip()
    blob = TextBlob(string).tokenize().stem()
    return [ word for word in blob ]

def split_to_sentences(doc):
    blob = TextBlob(doc)
    sentences = [sen.string for sen in blob.sentences]
    return sentences

def correct_drug_in_guesses(event, type):
    event_matches = []
    drugs = event[type]
    guesses_ids = [ document['guess_id'] for document in event['document']]
    for drug_id in drugs:
        if drug_id in guesses_ids:
            return True
    return False

def split_it(array):
    split_percentage = int(len(array)*0.8)
    training_data = array[:split_percentage]
    test_data = array[split_percentage:]
    return training_data, test_data

def split_by_drug(document):
    uniq_ids = set([x for y in document for x in y])
    res = []
    for id in uniq_ids:
        this_doc = { 'guess_id': id, 'positions' : [] }
        for sentence in document:
            if id in sentence:
                this_doc['positions'].append(1)
            else:
                this_doc['positions'].append(0)
        res.append(this_doc)
    return res

def resample(data, n=100):
    from scipy.interpolate import interp1d
    import numpy as np
    m = len(data)
    xin, xout = np.arange(n, 2*m*n, 2*n), np.arange(m, 2*m*n, 2*m)
    return interp1d(xin, data, 'nearest', fill_value='extrapolate')(xout)
