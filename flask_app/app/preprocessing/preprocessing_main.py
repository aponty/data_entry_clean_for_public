import math
from fuzzywuzzy import fuzz
from fuzzywuzzy import process
from nltk.corpus import stopwords
from app.preprocessing import helpers as help
import globals as g

stopwords = [ help.tokenize_sentence(word)[0] for word in stopwords.words('english') ]

exclusions = stopwords + [
    "care", "use", "natur", "group", "therapi", "standard", "treatment", "activ", "cancer", "medicin", "inhal", "pulmonari",
    "inject", "combin", "product", "indic", "medic", "specif", "subcutan", "form", "surfac", "inhibitor", "adjuv", "background", "contain",
    "convent", "like", "sequenc", "factor", "deep", "brain", "stimul", "control", "tumor", "wild", "type", "support", "suppli", "placebo",
    "mix", "patch", "whole", "human"
    ]

def format_data(data, type, training=False):
    data = [
        {
            'event_id': event['event_id'],
            'drugs': event['drugs'],
            'diseases': event['diseases'],
            'document': document_formatter(
                event['document'],
                type,
                event[type],
                training )
        } for event in data ]
    return data

def format_datum(document, type):
    datum = {
            'document': document_formatter(document, type)
            }
    return datum

def document_formatter(document, type, correct_ids=[], training=False):
    pick_type = {'diseases': g.G.diseases, 'drugs': g.G.drugs}
    options_tokenized, options = pick_type[type]()
    document = help.split_to_sentences(document)
    document = [ sentence_formatter(sentence, options_tokenized, options, training) for sentence in document ]
    document = help.split_by_drug(document)
    document = [ {
        'guess_id': doc['guess_id'],
        'positions': help.resample(doc['positions']),
        'correct': doc['guess_id'] in correct_ids # always false for single extractions as we don't already know the correct answer for those
        } for doc in document ]
    return document

def sentence_formatter(sentence, options_tokenized, options, training=False):
    flat_token_list = [ x for y in options_tokenized for x in y ]
    sentence = help.tokenize_sentence(sentence)
    sentence = [ word for word in sentence
        if word in flat_token_list and
        len(word) > 2 and
        word not in exclusions
    ]
    sentence = match_token_to_drug(sentence, options_tokenized, options, training)
    return sentence

def match_token_to_drug(sentence, options_tokenized, options, training=False):
    final_matches = []
    for token in sentence:
        token_matches = [] # may match with more than one drug; hence multiple filters
        for i, token_array in enumerate(options_tokenized):
            if token in token_array:
                token_matches.append(options[i])
            if training and len(token_matches) > 1: # say tokens are ['caps', 'patch']; this could match with capsasin (correct) and also capsildafil and capalasidasical and patchouli, and will match for all those in the same places of the document (same token)
            # so, using fuzz ratio to pick out just one. But if that misses, it misses entierly, and the NN has no chance of picking the right drug. Skip this, and NN has to pick out false positives.
            # NN can only decide on frequency and position, and those will be the same for all multiple matches of a token
            # SOLUTION: do this for training only. Don't do it for preprocessing docs that are actually being extracted, though.
                names = [ match['name'] for match in token_matches]
                best_guess = process.extractOne(token, names, scorer=fuzz.token_sort_ratio)[0]
                index = names.index(best_guess)
                token_matches = [ token_matches[index] ]
        for match in token_matches:
            final_matches.append(match)
    final_matches = [ match['id'] for match in final_matches ]
    return final_matches

# def resample(arr, new_length):
#     # diy interpolation, basically
#     # discussion/alternative @ my SO topic here
#     # https://stackoverflow.com/questions/50649875/mapping-arbitrary-length-list-to-fixed-length-preserving-frequency-and-position/50703675#50703675
#     arr_length = len(arr)
#     partition_size = arr_length/new_length
#     res = []
#     last_round_end_slice = 0
#     for i in range(new_length):
#         slice_start_index = int(math.floor(i * partition_size))
#         slice_end_index = int(math.ceil(i * partition_size))
#         if slice_start_index > last_round_end_slice: #sometimes an index will be skipped because rounding, this fills those gaps
#             slice_start_index = last_round_end_slice
#         if i == 0:
#             slice_end_index = int(math.ceil(partition_size))
#         if i == new_length:
#             slice_end_index = arr_length
#         partition = arr[slice_start_index:slice_end_index]
#         val = sum(partition)
#         if val > 0:
#             res.append(1)
#         else:
#             res.append(0)
#         last_round_end_slice = slice_end_index
#     return res
