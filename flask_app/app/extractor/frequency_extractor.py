import globals as g

def get_guess(string, type):
    pick_type = {'diseases': g.G.diseases, 'drugs': g.G.drugs}
    options = pick_type[type]()[1]
    res = {}
    for option in options:
        res[option['id']] = string.count(option['name'])
    max_val = max(res.values())
    res = [key for key,val in res.items() if val == max_val and max_val > 0 ]
    return res


def extract(agency, type, event, strict):
    string = event['document']
    guesses = get_guess(string, type)
    guesses = [ {
        'guess_id': guess,
        'prediction': True,
        'correct': guess in event[type]
        } for guess in guesses ]
    event.pop('document', None)
    event['guesses'] = guesses
    return event
