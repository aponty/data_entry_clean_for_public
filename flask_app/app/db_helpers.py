from numpy import save, load
from random import shuffle
import json
import urllib.request
import os
import globals as g

def get_and_save_or_open(agency, name, route):
    try:
        data = load(g.G.DB_PATH +'/'+ str(agency) + '/' + name).tolist()
    except FileNotFoundError:
        data = get(route)
        shuffle(data)
        save_it(agency, name, data)
    return data

def save_it(agency, name_of_file, array_or_dict_to_be_saved):
    path = g.G.DB_PATH + '/' + str(agency)
    if not os.path.exists(path):
        os.makedirs(path)
    path = path + '/' + name_of_file
    save(path, array_or_dict_to_be_saved)

def open_it(path, agency, name):
    path = g.G.DB_PATH + str(path)
    #oddly, tolist converts both lists and defaultdicts back to their correct type from the np type
    data = load(path + '/' + str(agency) + '/' + name).tolist()
    return data

def get(endpoint):
    ACCESS_TOKEN = os.environ.get('ACCESS_TOKEN')
    header = { "Authorization": "Bearer " + ACCESS_TOKEN }
    root_url = "https://app.contextmatters.com"
    # root_url = 'http://localhost:3000'
    api_base = "/api/v1/"
    url = root_url + api_base + endpoint
    req = urllib.request.Request(url, None, header)
    response = urllib.request.urlopen(req).read().decode('utf-8')
    return json.loads(response)
