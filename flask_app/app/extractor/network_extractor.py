import tensorflow as tf
import numpy as np
import globals as g

def extract(agency, type, event, strict):
    agency = str(agency)
    folder = agency + '/' + type
    if strict:
        export_path = g.G.DB_PATH + '/' + agency + '/' + type + '/strict_model/model'
    else:
        export_path = g.G.DB_PATH + '/' + agency + '/' + type + '/model/model'

    def neural_net(x, weights, biases):
        layer_1 = tf.add(tf.matmul(x, weights[0]), biases[0])
        layer_2 = tf.add(tf.matmul(layer_1, weights[1]), biases[1])
        out_layer = tf.add(tf.matmul(layer_2, weights[2]),  biases[2])
        return out_layer

    num_input   = 100
    n_hidden_1  = num_input     # 1st layer number of neurons
    n_hidden_2  = num_input     # 2nd layer number of neurons
    num_classes = 2             # result classes; # [0,1] == true and  [1, 0] == false.

    # placeholder variables, will be filled in with restored values from graph
    X = tf.placeholder(dtype=tf.float32, shape=[None, num_input])
    weights = {
        'h1': tf.Variable(tf.random_normal([num_input, n_hidden_1]), name='h1'),
        'h2': tf.Variable(tf.random_normal([n_hidden_1, n_hidden_2]), name='h2'),
        'out': tf.Variable(tf.random_normal([n_hidden_2, num_classes]), name='weight_out')
    }
    biases = {
        'b1': tf.Variable(tf.random_normal([n_hidden_1]), name='b1'),
        'b2': tf.Variable(tf.random_normal([n_hidden_2]), name='b2'),
        'out': tf.Variable(tf.random_normal([num_classes]),name='bias_out')
    }

    with tf.Session() as sess:
        sess.run(tf.global_variables_initializer())

        saver = tf.train.Saver()
        saver.restore(sess, export_path)
        graph = tf.get_default_graph()

        h1 = graph.get_tensor_by_name("h1:0")
        h2 = graph.get_tensor_by_name("h2:0")
        weight_out = graph.get_tensor_by_name("weight_out:0")
        b1 = graph.get_tensor_by_name("b1:0")
        b2 = graph.get_tensor_by_name("b2:0")
        bias_out = graph.get_tensor_by_name("bias_out:0")

        weights = [ h1, h2, weight_out ]
        biases = [ b1, b2, bias_out ]
        logits = neural_net(X, weights, biases)
        event = {
            'event_id': event.get('event_id', []),
            'drugs': event.get('drugs', []),
            'diseases': event.get('diseases', []),
            'guesses' : [{
                            'guess_id' : doc['guess_id'],
                            'prediction' : get_prediction(X, sess, logits, doc['positions']),
                            'correct' : doc['correct']
                        } for doc in event['document'] ]
            }
    tf.reset_default_graph()
    return event

def get_prediction(X, sess, logits, input_array):
    prediction = tf.nn.softmax(logits)
    single_test_feed = {X: [ input_array ]}
    classification = sess.run(prediction, single_test_feed)
    # [0,1] == true and  [1, 0] == false
    return int(classification[0].tolist()[1]) == 1
